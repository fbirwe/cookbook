const settings = require('./settings.json');

module.exports = () => {
    for ( let i in settings ) {
        if( !process.env[i] ) {
            process.env[i] = settings[i];
        }
    }
}