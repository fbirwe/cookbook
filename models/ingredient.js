const mongoose = require('mongoose');

mongoose.Promise = Promise;

// ingredient schema
const IngredientSchema = mongoose.Schema({
    /**
     * The singular of the name of the specific ingredient
     */     
    singular : {
        type : String,
        required : true,
        unique : true,
        index : true
    },
    unit : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Unit'
    },
    plural : String,
    fluid : Boolean,
    conversion : Number
})

// Model
const IngredientModel = mongoose.model('Ingredient', IngredientSchema);

const Ingredient = {
    // Create
    add : async ingredientJSON => {
        const ingredient = await IngredientModel.findOne({
            singular : ingredientJSON.singular
        });

        if( ingredient === null ) {
            const newIngredient = new IngredientModel(ingredientJSON);

            const res = await newIngredient.save();

            return res
        }

        return ingredient
    },
    // Read
    get : id => IngredientModel.findById(id),
    getAll : () => IngredientModel.find({}),
    // Update
    update : (id, args) => IngredientModel.findByIdAndUpdate(id, { $set : args }, { new : true }),
    // Delete
    delete : id => IngredientModel.findByIdAndRemove(id),
    deleteAll : () => IngredientModel.deleteMany({})
}

module.exports = Ingredient;