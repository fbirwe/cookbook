const mongoose = require('mongoose');
const helper = require('../helper/helper');

const Ingredient = require('./ingredient');
const Unit = require('./unit');
const Tag = require('./tag');
const User = require('./user');
const Category = require('./category');

mongoose.Promise = Promise;

// recipe schema
const RecipeSchema = mongoose.Schema({
    created : {
        type : Date,
        default : new Date()
    },
    modified : {
        type : Date,
        default : new Date()
    },
    title : {
        type : String,
        index : true
    },
    image : String,
    personCount : Number,
    ingredientGroups : [{
        groupTitle : String,
        ingredients : [{
            ingredient : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Ingredient'     
            },
            value : Number,
            unit : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Unit'
            }
        }]
    }],
    category : {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    description : [{
        title: String,
        body : {
            type : String,
            required : true
        }
    }],
    tags : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Tag'
    }],
    creator : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    preparationTime : Number,
    cookingTime : Number,
    restingTime : Number
});

RecipeSchema.index({
    title : 'text'
})

// Model
const RecipeModel = mongoose.model('Recipe', RecipeSchema);

async function getAll( populate ) {
    const recipes = await RecipeModel.find({})

    return  await Promise.all(recipes.map(async recipe => await prepareRecipe( recipe, populate ) ));
}

async function search( phrases, populate ) {
    // fields
    // title
    // tags
    // ingredients
    phrases = phrases.split(";");

    const WEIGHTS = {
        title : 5,
        tags : 1,
        ingredients : 1
    }


    const recipies = await RecipeModel.find({}, "title ingredientGroups tags")
    .populate({
        path : 'ingredientGroups.ingredients.ingredient',
        model : 'Ingredient'
    }).populate('tags')

    const weightTable = {}

    for( let i in phrases ) {
        const phrase = phrases[i];

        for( recipe of recipies ) {
            let weight = 0

            // title
            if( recipe.title.toLowerCase().includes(phrase.toLowerCase()) ) {
                weight += WEIGHTS.title;
            }

            // tags
            weight += recipe.tags.reduce((prev, cur) => {
                return cur.title.toLowerCase().includes(phrase.toLowerCase()) ? prev + WEIGHTS.tags : prev;
            }, 0)


            // ingredients
            weight += recipe.ingredientGroups.reduce((prev, cur) => {
                return prev + cur.ingredients.reduce((prevInner, curInner) => {
                    return curInner.ingredient.singular.toLowerCase().includes(phrase.toLowerCase()) ? prevInner + WEIGHTS.ingredients : prevInner;
                }, 0)
            }, 0)

            if( weight > 0 ) {
                if ( !weightTable[ recipe._id ] ) {
                    weightTable[ recipe._id ] = 0
                }

                weightTable[ recipe._id ] += weight;
            }

        }
    }

    const out = await RecipeModel.find({
        _id : Object.keys(weightTable)
    })

    out.map(el => {
        console.log(`${weightTable[el._id]}: ${el._id}`)
    })

    out.sort((a, b) => {
        return weightTable[b._id] - weightTable[a._id]
    })

    return  await Promise.all(out.map(async recipe => await prepareRecipe( recipe, populate ) ));
}

async function prepareRecipe( recipe, populate ) {
    if( !populate ) {
        return recipe._id;
    }    

    // populate
    recipe =  await recipe.populate({
        path : 'ingredientGroups.ingredients.ingredient',
        model : 'Ingredient'
    }).populate({
        path : 'ingredientGroups.ingredients.unit',
        model : 'Unit'
    }).populate('tags')
    .populate('category')
    .execPopulate()


    recipe = recipe.toJSON();

    delete recipe['__v'];

    return recipe;
}

async function createCategory( recipe ) {
    if ( recipe.category ) {
        if( !helper.isObjectID(recipe.category) ) {
            const title = typeof(recipe.category) === 'string' ? recipe.category : recipe.category.title

            const category = await Category.add({
                title
            });

            recipe.category = category._id
        }
    }

    return recipe;
}

async function createIngredients( recipe ) {
    if ( recipe.ingredientGroups ) {
        for( let i in recipe.ingredientGroups ) {
            const ingredientGroup = recipe.ingredientGroups[i];
    
            // create new Ingredients and Units
            for ( let j in ingredientGroup.ingredients ) {
    
                // create ingredient
                if( ingredientGroup.ingredients[j].ingredient !== undefined && !helper.isObjectID( ingredientGroup.ingredients[j].ingredient ) ) {
                    const singular = typeof(ingredientGroup.ingredients[j].ingredient) === 'string' ? ingredientGroup.ingredients[j].ingredient : ingredientGroup.ingredients[j].ingredient.singular
                    
                    const ingredient = await Ingredient.add({
                        singular
                    });
    
                    ingredientGroup.ingredients[j].ingredient = ingredient._id;
                }
    
                // create unit
                if( ingredientGroup.ingredients[j].unit !== undefined && !helper.isObjectID( ingredientGroup.ingredients[j].unit ) ) {

                    const title = typeof(ingredientGroup.ingredients[j].unit) === 'string' ? ingredientGroup.ingredients[j].unit : ingredientGroup.ingredients[j].unit.title;

                    if( title === '' ) {
                        delete ingredientGroup.ingredients[j].unit
                    } else {
                        const unit = await Unit.add({
                            title
                        })
        
                        ingredientGroup.ingredients[j].unit = unit._id;
                    }

                }    
            }

        }
    }

    // delete ids
    const deleteId = obj => {

    }

    return recipe
}

async function createTags( recipe ) {
    if( recipe.tags ) {
        for( let i in recipe.tags ) {
            if( !helper.isObjectID(recipe.tags[i]) ) {
                const title = typeof(recipe.tags[i]) === 'string' ? recipe.tags[i] : recipe.tags[i].title

                const tag = await Tag.add({
                    title
                });
    
                recipe.tags[i] = tag._id;
            }
        }    
    }

    return recipe
}

const Recipe = {
    // Create
    add : async (recipeJSON, populate) => {
        recipeJSON = await createIngredients( recipeJSON );
        recipeJSON = await createTags( recipeJSON );
        recipeJSON = await createCategory( recipeJSON );

        const recipe = new RecipeModel(recipeJSON);
        const res = await recipe.save();

        return await prepareRecipe(res, populate);

    },
    // Read
    get : async ( id, populate ) => {
        const recipe = await RecipeModel.findById(id)

        return await prepareRecipe( recipe, populate )
    },
    getAll,
    search,
    // Update
    update : async (id, args) => {
        args = await createIngredients( args );
        args = await createTags( args );
        args = await createCategory( args );
        args.modified = new Date();

        return await RecipeModel.findByIdAndUpdate(id, { $set : args }, { new : true })
    },
    // Delete
    delete : async (id, populate) => {
        const recipe = await RecipeModel.findByIdAndRemove(id);

        return await prepareRecipe(recipe);
    },
    deleteAll : async (populate) => {
        const recipes = await RecipeModel.deleteMany({});

        return recipes

        // return  await Promise.all(recipes.map(async recipe => await prepareRecipe( recipe, populate ) ));
    }
}

module.exports = Recipe;