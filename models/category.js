const mongoose = require('mongoose');
mongoose.Promise = Promise;

// tag schema
const CategorySchema = mongoose.Schema({
    title : {
        type : String,
        required : true,
        unique : true,
        index : true
    }
});

// Model
const CategoryModel = mongoose.model('Category', CategorySchema);


async function prepareCategory( category, populate=false ) {
    if( !populate ) {
        return category._id;
    }

    category = category.toJSON();

    delete category['__v'];

    return category
}


const Category = {
    // Create
    add : async (categoryJSON, populate) => {
        const category = new CategoryModel(categoryJSON);

        try {
            const res = await category.save();

            return await prepareCategory(res, populate);
        } catch(e) {
            const cat = await CategoryModel.findOne({
                title : categoryJSON['title']
            })

            return await prepareCategory(cat, populate);
        }
    },
    // Read
    get : async (id, populate) => {
        const category = await CategoryModel.findById(id);

        return await prepareCategory( category, populate )
    },
    getAll : async (populate) => {
        const categories = await CategoryModel.find({})

        return await Promise.all(categories.map(async category => await prepareCategory( category, populate ) ));
    },
    // Update
    update : (id, args) => CategoryModel.findByIdAndUpdate(id, { $set : args }, { new : true }),
    // Delete
    delete : async (id, populate) => {
        const category = await CategoryModel.findByIdAndRemove(id);

        return await prepareCategory(category, populate)
    },
    deleteAll : async (populate) => {
        const categories = await CategoryModel.remove({});

        return await Promise.all(categories.map(async category => await prepareCategory( category, populate ) ));
    }
}

module.exports = Category;