const mongoose = require('mongoose');
mongoose.Promise = Promise;
const { normalizeName } = require('../helper/helper');

// tag schema
const TagSchema = mongoose.Schema({
    title : {
        type : String,
        required : true,
        unique : true,
        index : true
    },
    normalizedName : {
        type : String,
        required : true,
        unique : true,
        index : true        
    },
    image : String
});

// Model
const TagModel = mongoose.model('Tag', TagSchema);

const Tag = {
    // Create
    add : async (tagJSON) => {

        const normalizedName = normalizeName( tagJSON.title );

        const data = await TagModel.findOne({ normalizedName })

        if( !data ) {
            tagJSON.normalizedName = normalizedName;
            const tag = new TagModel(tagJSON);

            return await tag.save();
        }

        return data
    },
    // Read
    get : id => TagModel.findById(id),
    getByNormalizedName : name => TagModel.findOne({ normalizedName : name }),
    getAll : () => TagModel.find({}),
    // Update
    update : (id, args) => TagModel.findByIdAndUpdate(id, { $set : args }, { new : true }),
    // Delete
    delete : id => TagModel.findByIdAndRemove(id),
    deleteAll : () => TagModel.remove({})
}

module.exports = Tag;