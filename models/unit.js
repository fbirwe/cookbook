const mongoose = require('mongoose');
mongoose.Promise = Promise;

const { normalizeName } = require('../helper/helper');

// unit schema
const UnitSchema = mongoose.Schema({
    title : {
        type : String,
        required : true,
        unique : true,
        index : true
    },
    normalizedName : {
        type : String,
        required : true,
        unique : true,
        index : true
    },
    /**
     * is the ingredient a fluid ingedrient
     */
    fluid : Boolean,
    conversion : Number
})

// Model
const UnitModel = mongoose.model('Unit', UnitSchema);

const Unit = {
    // Create
    add : unitJSON => {
        return new Promise((res, rej) => {
            const normalizedName = normalizeName( unitJSON.title );

            UnitModel.findOne({ 
                normalizedName
            })
            .then(data => {                
                if( data === null ) {
                    // console.log(normalizedName, unitJSON);
                    unitJSON.normalizedName = normalizedName;
                    const unit = new UnitModel(unitJSON);

                    unit.save((err, results) => {
                        if(err) {
                            rej(err);
                        } else {
                            res(results);
                        }
                    });
                } else {
                    res(data);
                }
            })
            .catch(err => rej(err));
        });
    },
    // Read
    get : id => UnitModel.findById(id),
    getByNormalizedName : name => UnitModel.findOne({ normalizedName : name }),
    getAll : () => UnitModel.find({}),
    // Update
    update : (id, args) => UnitModel.findByIdAndUpdate(id, { $set : args }, { new : true }),
    // Delete
    delete : id => UnitModel.findByIdAndRemove(id),
    deleteAll : () => UnitModel.remove({})
}

module.exports = Unit;