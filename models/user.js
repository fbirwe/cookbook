let mongoose = require('mongoose');
const Cookbook = require('./cookbook');
mongoose.Promise = Promise;

// user schema
const UserSchema = mongoose.Schema({
    created : {
        type : Date,
    },
    modified : {
        type : Date
    },
    email : {
        type : String,
        required : true,
        index : true,
        unique : true
    },
    firstname : String,
    lastname : String,
    password : String,
    username : String,
    avatar : String,
    isLocked : {
        type : Boolean,
        default : true
    },
    role : {
        type : String,
        default : "member"
    },
    cookbooks : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Cookbook',
    }],
    defaultCookbook : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Cookbook'
    }
})

// Model
const UserModel = mongoose.model('User', UserSchema);

const prepareUser = (user, populate, complete=false ) => {
    user = user.toJSON();

    if( !populate ) {
        return user._id;
    }

    if ( !complete ) {
        delete user['password'];
        delete user['__v'];    
    }

    return user;
}

async function addDefaultCookbook( user ) {
    let title = user.email

    if( user.username ) {
        title = user.username;
    } else if ( user.firstname ) {
        title = user.firstname;
    }

    const newCookbook = await Cookbook.add({
        title : `${title}s Kochbuch`,
        owner : user._id
    })

    const args = {
        cookbooks : user.cookbooks ? [...user.cookbooks, newCookbook._id] : [newCookbook._id],
        defaultCookbook : newCookbook._id
    }

    await User.update( user.id, args )

}

const User = {
    // Create
    add : async (userJSON, populate=false ) => {
        // remove security fields
        delete userJSON['isLocked']
        delete userJSON['role']

        // set defaults
        userJSON['created'] = new Date();
        userJSON['modified'] = new Date();

        // add default cookbook
        const user = new UserModel(userJSON);
        const res = await user.save();

        await addDefaultCookbook( res );

        return prepareUser( res, populate );

    },
    // Read
    get : async ( id, populate=false, complete=false ) => {
        const user = await UserModel.findById(id);

        return prepareUser(user, populate, complete);
    },
    getByMail : async (mail, populate=false, complete=false ) => {
        const user = await UserModel.findOne({
            email : mail
        });

        return prepareUser(user, populate, complete);
    },
    getAll : async ( populate=false ) => {
        const users = await UserModel.find({});

        return users.map((user) => prepareUser(user, populate));
    },
    // Update
    update : async (id, args, populate=false) => {
        console.log( args )

        if( args.password ) {
            delete args.password;
        }
        if( args.email ) {
            delete args.email;
        }


        const user = await UserModel.findByIdAndUpdate(id, { $set : args }, { new : true })

        return prepareUser(user);
    },
    // Delete
    delete : async (id, populate=false ) => {
        const user = await UserModel.findByIdAndRemove(id)

        return prepareUser(user, populate)
    },
    deleteAll : () => async ( populate=false ) => {
        const users = await UserModel.remove({});

        return users.map(user => prepareUser(user, populate));
    }
}

module.exports = User;