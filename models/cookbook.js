const mongoose = require('mongoose');
const { isObjectID } = require('../helper/helper');

mongoose.Promise = Promise;


// cookbook schema
const CookbookSchema = mongoose.Schema({
    title : String,
    recipes : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Recipe',
        default : []
    }],
    // collections : [{
    //     type : mongoose.Schema.Types.ObjectId,
    //     ref : 'Collection',
    //     default : []
    // }],
    owner : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    }
})

// Model
const CookbookModel = mongoose.model('Cookbook', CookbookSchema);

function prepareCookbook( cookbook, populate ) {
    cookbook = cookbook.toJSON();

    if( !populate ) {
        return cookbook._id;
    }

    delete cookbook['__v'];

    return cookbook;
}

const Cookbook = {
    // Create
    add : async (cookbookJSON, populate) => {
        const cookbook = new CookbookModel(cookbookJSON);
        const res = await cookbook.save();

        return prepareCookbook( res, populate );
    },
    // Read
    get : async (id, populate) => {
        const cookbook = await CookbookModel.findById(id);

        return prepareCookbook(cookbook, populate);
    },
    getByRecipe : async ( recipeId, populate ) => {
        const cookbook = await CookbookModel.find({
            recipeId : { $in : recipes }
        })

        return prepareCookbook(cookbook, populate);
    },
    getAll : async ( populate ) => {
        const cookbooks = await CookbookModel.find({})

        return cookbooks.map( cookbook => prepareCookbook( cookbook, populate ) );
    },
    // Update
    update : async (id, args, populate) => {
        const cookbook = await CookbookModel.findByIdAndUpdate(id, { $set : args }, { new : true });

        return prepareCookbook(cookbook, populate);
    },
    addRecipe : async (id, recipeId, populate) => {
        const cookbook = await CookbookModel.findById(id);

        const { recipes } = cookbook;


        if( !recipes.map(el => el.toString()).includes( recipeId ) ) {
            const updatedCookbook = await CookbookModel.findByIdAndUpdate(id, { $set : {
                recipes : [...recipes, recipeId]
            } }, { new : true });

            return prepareCookbook( updatedCookbook, populate );
        }

        return prepareCookbook( cookbook, populate);
    },
    removeRecipe : async (id, recipeId, populate) => {
        const cookbook = await CookbookModel.findById(id);

        const { recipes } = cookbook;

        const updatedRecipes = recipes.filter(el => el._id.toString() !== recipeId);

        if( updatedRecipes.length < recipes.length ) {
            const updatedCookbook = await CookbookModel.findByIdAndUpdate(id, { $set : {
                recipes : updatedRecipes
            } }, { new : true });

            return prepareCookbook( updatedCookbook, populate );
        }

        return prepareCookbook( cookbook, populate);
    },
    // Delete
    delete : async (id, populate) => {
        const cookbook = await CookbookModel.findByIdAndRemove(id);

        return prepareCookbook( cookbook, populate );
    },
    deleteAll : async (populate) => {
        const cookbooks = await CookbookModel.deleteMany({});

        return cookbooks.map( cookbook => prepareCookbook( cookbook, populate ) );
    }
}

module.exports = Cookbook;