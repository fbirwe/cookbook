FROM node:10

# Create app directory
WORKDIR /usr/src/app

RUN npm install -g nodemon

COPY package*.json ./

RUN npm install

COPY . .

RUN chmod +x ./wait-for-it.sh

EXPOSE 8082

CMD [ "node", "index.js" ]