function createHash(length) {
    let hash = "";

    for( let i = 0; i < length; i++) {
        // 33 - 126
        hash += String.fromCharCode( Math.round(Math.random() * 93) + 33 )
    }

    return hash;
}

module.exports.createHash = createHash;