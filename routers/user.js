const express = require('express');
const router = express.Router();

const User = require('../models/user');
const handleImages = require('../helper/handleImages');
const multer = require('multer');
const upload = multer();

// CREATE
router.post('/', async (req, res, next) => {
    const { user } = req.body;

    try {
        const newUser = await User.add( user, req.populate );

        res.json( newUser );
    } catch(err) {
        next(err);
    }
});

// READ
router.get('/', (req, res, next) => {

    User.getAll( req.populate )
    .then(data => {
        return res.json(data)
    })
    .catch(err => next(err))
})

router.get('/:id/avatar', (req, res, next) => {
    const { id } = req.params;

    User.get( id, true )
    .then(async data => {
        const imageName = data.avatar ? data.avatar : process.env.DEFAULT_USER_IMAGE;
        
        console.log( imageName );

        const imageBuffer = await handleImages.loadImage( imageName )
        const extension = imageName.split('.')[imageName.split('.').length - 1];

        res.contentType( handleImages.getMimeTypeByExtension(extension) );
        res.send( imageBuffer );

    })
    .catch(err => next(err));
})

router.get('/logged_in', (req, res, next) => {
    const {user} = req

    res.json(user)
})

router.get('/:id', (req, res, next) => {
    const { id } = req.params;

    User.get(id, req.populate )
    .then(data => res.json(data))
    .catch(err => next(err));
})

router.get('/email/:email', (req, res, next) => {
    const { email } = req.params;

    User.getByMail(email, req.populate )
    .then(data => res.json(data))
    .catch(err => next(err));
})


// Update
router.put('/:id', (req, res, next) => {
    const { id } = req.params;
    console.log( "the body: ", req.body )

    const { user } = req.body;

    User.update(id, user, req.populate)
    .then(data => res.json(data))
    .catch(err => next(err));
})

router.put('/:id/avatar', upload.single('image'), (req, res, next) => {
    const { id } = req.params;
    const { buffer, mimetype } = req.file;
    
    handleImages.storeImage( buffer, handleImages.getExtensionByMimeType( mimetype ) )
    .then(name => {
        User.update( id , { avatar : name })
        .then(ok => res.sendStatus(200))
        .catch(err => next(err));
    })
    .catch(err => {
        next(err);
    })

})

// Delete
router.delete('/:id', (req, res, next) => {
    const { id } = req.params;

    User.delete(id, req.populate )
    .then(data => res.json(data))
    .catch(err => next(err));
});

module.exports = router;