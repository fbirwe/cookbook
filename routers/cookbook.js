const express = require('express');
const router = express.Router();

const Cookbook = require('../models/cookbook');
const { cookbookIsAllowed } = require('../middleware/checkPermissions');

// CREATE
router.post('/', (req,res,next) => {
    const { cookbook } = req.body;

    Cookbook.add({
        ...cookbook,
        owner : req.user._id
    }, req.populate)
    .then(data => res.json(data))
    .catch( err => next(err) )
})

// READ
router.get('/', (req, res, next) => {
    Cookbook.getAll( req.populate )
    .then(async data => {
        const isAllowed = await Promise.all(data.map( async cookbook => await cookbookIsAllowed(req.userId,cookbook)))
        const filteredData = data.filter((val, i) => isAllowed[i])
        res.json(filteredData)

    })
    .catch( err => next(err) );
})

router.get('/:id', (req, res, next) => {
    const { id } = req.params;

    Cookbook.get(id, req.populate)
    .then(async data => {
        if( await cookbookIsAllowed(req.userId, data) === false ) {
            throw new Error("permission denied");
        }

        res.json(data)
    })
    .catch( err => next(err) );
})

// UPDATE
router.put('/:id', (req, res, next) => {
    const { id } = req.params;
    const { cookbook } = req.body;

    console.log( id, cookbook );

    Cookbook.update(id, cookbook, req.populate)
    .then(data => res.json(data))
    .catch( err => next(err) );
})

router.put('/:id/add_recipe/:recipeId', (req, res, next) => {
    const { id, recipeId } = req.params;
    
    Cookbook.addRecipe(id, recipeId, req.populate)
    .then(data => res.json(data))
    .catch( err => next(err) );
})

router.put('/:id/remove_recipe/:recipeId', (req, res, next) => {
    const { id, recipeId } = req.params;
    
    console.log("blub")

    Cookbook.removeRecipe(id, recipeId, req.populate)
    .then(data => res.json(data))
    .catch( err => next(err) );
})

module.exports = router;