const express = require('express');
const router = express.Router();
const Recipe = require('../models/recipe');
const Stopwatch = require('../helper/stopwatch');
const handleImages = require('../helper/handleImages');
const { recipeIsAllowed } = require('../middleware/checkPermissions');
const multer = require('multer');
// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
})
   
const upload = multer()

// CREATE
router.post('/', (req, res, next) => {
    const { recipe } = req.body;
    recipe.creator = req.userId;

    Recipe.add(recipe, req.populate )
    .then(data => res.json(data))
    .catch(err => next(err));
})

// READ
router.get('/', (req, res, next) => {
    let promise = null
    const stopwatch = new Stopwatch();

    if( req.query.search ) {
        const phrases = req.query.search

        promise = Recipe.search( req.query.search, req.populate )
    } else {
        promise = Recipe.getAll( req.populate );
    }

    promise
    .then(async data => {
        stopwatch.addRound();

        const isAllowed = await Promise.all(data.map( async recipe => await recipeIsAllowed(req.userId,recipe)))
        stopwatch.addRound();
        
        const filteredData = data.filter((val, i) => isAllowed[i])
        stopwatch.addRound();
        
        stopwatch.print();
        res.json(filteredData)
    })
    .catch(err => next(err));
})

router.get('/:id', (req, res, next) => {
    const { id } = req.params;

    Recipe.get( id, req.populate )
    .then(data => res.json(data))
    .catch(err => next(err));
})

router.get('/:id/image', (req, res, next) => {
    const { id } = req.params;

    Recipe.get( id, true )
    .then(async data => {
        const imageName = data.image ? data.image : process.env.DEFAULT_RECIPE_IMAGE;
        
        console.log( imageName );

        const imageBuffer = await handleImages.loadImage( imageName )
        const extension = imageName.split('.')[imageName.split('.').length - 1];

        res.contentType( handleImages.getMimeTypeByExtension(extension) );
        res.send( imageBuffer );

    })
    .catch(err => next(err));
})

// UPDATE
router.put('/:id', (req, res, next) => {
    const { id } = req.params;
    const { recipe } = req.body;

    console.log( id, req.body )

    Recipe.update(id, recipe, req.populate)
    .then(data => res.json(data))
    .catch( err => next(err) );
})

router.put('/:id/image', upload.single('image'), (req, res, next) => {
    const { id } = req.params;
    const { buffer, mimetype } = req.file;
    
    handleImages.storeImage( buffer, handleImages.getExtensionByMimeType( mimetype ) )
    .then(name => {
        Recipe.update( id , { image : name })
        .then(ok => res.sendStatus(200))
        .catch(err => next(err));
    })
    .catch(err => {
        next(err);
    })

})

// DELETE
router.delete('/:id', (req, res, next) => {
    const { id } = req.params;

    Recipe.delete( id, req.populate )
    .then( data => res.json(data) )
    .catch( err => next(err) );
})

router.delete('/', (req, res, next) => {

    Recipe.delete( req.populate )
    .then( data => res.json(data) )
    .catch( err => next(err) );
})

module.exports = router;