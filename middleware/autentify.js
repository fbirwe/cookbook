const express = require('express');
const bodyParser = require('body-parser')
const Router = express.Router();
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const bcrypt = require('bcrypt-nodejs');

class Helper {
    constructor({jwt, secret}){
        this.jwt = jwt;
        this.secret = secret;
    }

    confirmUser(req, res, next) {
        let id = req.params.id;
    
        User.get(id).exec((err, user) => {
            if(err) {
                res.sendStatus(500);
            } else {
                if(user === null) {
                    res.sendStatus(404);
                } else {
                    user.confirmed = true;
    
                    user.save((err, results) => {
                        if (err) {
                            res.sendStatus(500);
                        } else {
                            res.sendStatus(200);
                        }
                    });  
                }
            }
        });
    }

    async login(req, res, next) {
        const { autentify } = req;

        console.log('in login')

        if(req.body.email === undefined || req.body.password === undefined){
            res.sendStatus(400);
            return;
        }

        try {
            const data = await User.getByMail(req.body.email, true, true)
            // is User in DB?
            if(data === null) {
                res.sendStatus(404);
                return;
            }

            console.log(req.body.password, data)
            // Compare Password to Password in DB
            bcrypt.compare(req.body.password, data.password, (err_bcrypt, isConfirmed) => {
                if (err_bcrypt){
                    res.sendStatus(403);
                }else{
                    console.log("all in all it seems oK", isConfirmed)

                    if(isConfirmed){
                        let tempUser = {...data};
                        delete tempUser.password;

                        // generate and send jwt-Token
                        // Expires in als 3. Parameter: { expiresIn : '30s' }
                        autentify.jwt.sign({ user : tempUser }, autentify.secret, (err_jwt, token) => {
                            if (err_jwt) {
                                res.sendStatus(500);
                            }else{
                                res.json({
                                    token,
                                })    
                            }
                        }); 
                    }else{
                        res.sendStatus(403);
                    }
                }                  
            })   
        } catch(e) {
            console.error( e );

            res.sendStatus(403)
        }
    }

    register(req, res, next) {
        const { autentify } = req;
        const { user } = req.body;

        bcrypt.hash(user.password, null, null, (err, hash) => {
            if(err) {
                res.sendStatus(400);
            } else {
                user.password = hash;

                User.add(user)
                .then(userSaved => {
                    autentify.jwt.sign({user : userSaved}, autentify.secret, { expiresIn : '1y' }, (err, token) => {
                        if(err) {
                            res.sendStatus(400);
                        }
                        
                        res.json({
                            token
                        })
                    })
                })
                .catch(err => {
                    console.log(err);

                    res.sendStatus(400);
                });
            }
        });
    }

    setPassword(req, res, next) {
        if(req.body.email !== undefined && req.body.password !== undefined){
            User.getByMail(req.body.email)
            .then(user => { 
                // encrypt password
                bcrypt.hash(req.body.password, null, null, (err, hash) => {
                    if (err) {
                        res.status(400).send(err);
                    }else{
                        User.update(user._id, hash)
                        .then(updatedUser => {
                            res.json(updatedUser);
                        })
                        .catch(err => {
                            res.status(400).send(err);
                        })
                    }          
                })
            })
        }
    }

    // Verify Token
    // FORMAT OF TOKEN
    // Auhtorization: Bearer <access_token>
    verifyUser(req, res, next) {
        // Verify Token
        // Get the auth header value
        const bearerHeader = req.headers['authorization'];

        // check if bearer is undefined
        if(typeof bearerHeader !== 'undefined') {
            // Split at the space and get second value
            const bearerToken = bearerHeader.split(' ')[1];
            // Set the token
            req.token = bearerToken;


            // verify
            this.jwt.verify(bearerToken, this.secret, (err, authData) => {
                if(err) {      
                    res.sendStatus(403);
                } else {
                    // Authdata enthält die in Sign übergebenen user-Daten

                    req.userId = authData.user._id;
                    req.user = authData.user;
                    req.authData = authData;

                    User.get(req.userId)
                    .then(data => {
                        next();
                    })
                    .catch(err => {
                        res.sendStatus(404);
                    })
                }
            });
        } else {
            // Forbidden
            res.sendStatus(403);
        }
    }

    // is User confirmed?
    isUserConfirmed(req, res, next) {
        if(req.authData !== undefined && req.user.confirmed) {
            next();
        } else {
            res.sendStatus(403);
        }
    }

}

module.exports = Helper;