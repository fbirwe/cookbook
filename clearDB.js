const mongoose = require('mongoose');
const User = require('./models/user');
const Cookbook = require('./models/cookbook');
const Recipe = require('./models/recipe');
const Unit = require('./models/unit');
const Tag = require('./models/tag');
const Ingredient = require('./models/ingredient');

const dotenv = require('dotenv');
dotenv.config();

test();


async function test() {
    try {
        await mongoose.connect( `mongodb://db:27017/${process.env.db_name}`, { useNewUrlParser: true } )
        await Recipe.deleteAll();
        await Unit.deleteAll();
        await Ingredient.deleteAll();
        await Tag.deleteAll();
        await Cookbook.deleteAll();
        // await User.deleteAll();        
    } catch(e) {
        console.log(e);
    }

    // console.log('db cleared');
    // process.exit(0);
}

module.exports = {
    test
}