const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const autentifyHelper = require('./middleware/autentify');
const autentify = new autentifyHelper({
    jwt,
    secret : process.env.HASH
})
const { isUserLoggedIn } = require('./middleware/autentifyUser');


const userHandlingRouter = require('./routers/userHandling');

const imageRouter = require('./routers/image');
const cookbookRouter = require('./routers/cookbook')
const recipeRouter = require('./routers/recipe');
const userRouter = require('./routers/user');
const categoryRouter = require('./routers/category');

const sharedLinkRouter = require('./routers/sharedLink');

// allgemeine Middlewarefunktionen
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

app.use('/api', (req, res, next) => {
    console.log( req.originalUrl )
    next();
})

app.get('/api', (req, res, next) => {
    res.json({
        test : "test"
    })
})

app.use('/api', (req, res, next) => {
    try {
        next();
    } catch(err) {
        res.send(err);
    }
})

app.use( '/api', (req, res, next) => {
    req.autentify = autentify;

    next()
})

app.use('/api/image', isUserLoggedIn );
app.use('/api/image', imageRouter)

app.use('/api/data', isUserLoggedIn );

app.use( '/api/data', (req, res, next) => {
    if( req.query.populate ) {
        req.populate = req.query.populate.trim().toLowerCase() === 'true';
    } else {
        req.populate = false;
    }

    next();
})

app.post('/api/data/scrape', require('./middleware/scrapeChefkoch'));

app.use('/api/data/cookbook', cookbookRouter);
app.use('/api/data/recipe', recipeRouter);
app.use('/api/data/user', userRouter);
app.use('/api/data/category', categoryRouter);

app.use('/api/shared', sharedLinkRouter)

// app.use('/', userHandlingRouter);
// Register, login, logout

app.post('/api/register', autentify.register)
app.post('/api/login', autentify.login)


connectToDB(5,3)

function connectToDB(remainingAttempts, secondsToSleep) {
    let dbLogin = null;

    if( process.env.MODE === 'dev' ) {
        dbLogin = mongoose.connect( `mongodb://${process.env.DB_HOST}:27017/${process.env.DB_NAME}`, { 
            useNewUrlParser: true        
        } )
    } else {
        dbLogin = mongoose.connect( `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:27017/${process.env.DB_NAME}`, { 
            useNewUrlParser: true
        } )

        // dbLogin= mongoose.connect( `mongodb://${process.env.DB_HOST}:27017/${process.env.DB_NAME}`, { 
        //     useNewUrlParser: true,
        //     user : process.env.DB_USER,
        //     pass : process.env.DB_PASSWORD
        // } )
    }

    if( dbLogin ) {
        dbLogin.then(() => {
            console.log('connection to db established');
            app.listen(process.env.PORT || 5000, '0.0.0.0', (err) =>{
                if(err){
                    console.log(err);
                } else {
                    console.log(`server listening on port ${process.env.PORT}`);
                }
            });
        })
        .catch((err) => {
            console.log(`Connection failed, remaining attempts: ${remainingAttempts}`);
            console.error(err);
    
            if(remainingAttempts > 0) {
                setTimeout(() => {
                    connectToDB(remainingAttempts - 1, secondsToSleep);
                }, secondsToSleep * 1000)
            }
        });
    }
}